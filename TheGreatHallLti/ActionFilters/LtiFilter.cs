﻿using LtiLibrary.Extensions;
using LtiLibrary.Lti1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace TheGreatHallLti.ActionFilters
{
    public class LtiFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase request = filterContext.RequestContext.HttpContext.Request;
            dynamic viewBag = filterContext.Controller.ViewBag;

            //try
            //{
                request.CheckForRequiredLtiParameters();
                var ltiRequest = new LtiRequest(null);
                ltiRequest.ParseRequest(request);

                var secret = "secret";
                var oauthSignature = request.GenerateOAuthSignature(secret);

                if (oauthSignature.Equals(ltiRequest.Signature))
                {
                    filterContext.RouteData.Values.Add("request", ltiRequest);
                }
                else
                {
                    filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                    filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "Error401",
                    };
                }

            //}
            //catch (Exception e)
            //{
            //    filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            //    filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            //    filterContext.Result = new ViewResult
            //    {
            //        ViewName = "Error403",
            //    };
            //}
        }
    }
}