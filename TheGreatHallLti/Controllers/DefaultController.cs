﻿using LtiLibrary.Extensions;
using LtiLibrary.Lti1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheGreatHallLti.ActionFilters;
using TheGreatHallLti.Models;

namespace TheGreatHallLti.Controllers
{
    public class DefaultController : Controller
    {
        // POST: Default
        //[LtiFilter]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        //[LtiFilter]
        //[HttpPost]
        public ActionResult LtiTest()
        {
            try
            {
                Request.CheckForRequiredLtiParameters();

                var ltiRequest = new LtiRequest(null);
                ltiRequest.ParseRequest(Request);

                if (!ltiRequest.ConsumerKey.Equals("12345"))
                {
                    ViewBag.Message = "Invalid Consumer Key";
                    return View();
                }

                var oauthSignature = Request.GenerateOAuthSignature("secret");
                if (!oauthSignature.Equals(ltiRequest.Signature))
                {
                    ViewBag.Message = "Invalid Signature";
                    return View();
                }

                // The request is legit, so display the tool
                ViewBag.Message = string.Empty;
                var model = new ToolModel
                {
                    ConsumerSecret = "secret",
                    LtiRequest = ltiRequest
                };
                return View(model);
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View();
            }
        }
    }
}