﻿using LtiLibrary.Lti1;

namespace TheGreatHallLti.Models
{
    public class ToolModel
    {
        public string ConsumerSecret { get; set; }
        public LtiRequest LtiRequest { get; set; }
    }
}